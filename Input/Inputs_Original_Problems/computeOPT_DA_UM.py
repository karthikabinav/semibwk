import sys
import numpy as np
from numpy import array
import math
import cvxopt
from cvxopt import matrix,spmatrix, solvers
import random

n=26
T=int(sys.argv[1])
B=T/2
delta=0.01
trial=sys.argv[2]

d=1
K=2

means_f=open('../DynamicAssortment/Means_' + str(T) + '_' + trial +  '.in', 'r')

means=[[0 for i in xrange(d+1)] for i in xrange(n)]

i=0
for line in means_f:
  vals=line.split(",")
  j=0
  for val in vals:
    means[i][j] = float(val.split("\n")[0])
    j+=1
  i+=1

#Construct LP
W=list()
for i in xrange(n):
  W.append(-means[i][0])

vals=list()
I=list()
J=list()

b=list()
#LCB constraint
for i in xrange(d):
  b.append(B/float(T))
  for j in xrange(n):
    vals.append(means[j][i+1])
    I.append(i)
    J.append(j)

#Matroid constraint
b.append(K)
for j in xrange(n):
  vals.append(1)
  I.append(d)
  J.append(j)

#Variable bounds constraint
for j in xrange(n):
  b.append(1)
  vals.append(1)
  I.append(d+j+1)
  J.append(j)

for j in xrange(n):
  b.append(0)
  vals.append(-1)
  I.append(d+n+1+j)
  J.append(j)

A=spmatrix(vals, I, J)
Wc=matrix(W)
bc=matrix(b)

#Solve LP
solvers.options['show_progress'] = False
sol=solvers.lp(Wc, A, bc) 
print -T*float(sol['primal objective'])
