import sys
import numpy as np
import random
from scipy.stats import truncnorm

#n=int(sys.argv[1])
n=26
T=int(sys.argv[1])
d=1
trial=int(sys.argv[2])


num_products=2
mu=np.random.random(num_products)

X=[[0 for i in xrange(d+1)] for j in xrange(n*T)]


atom=0
f=open('../DynamicPricing/Feedback_' + str(T) + '_' + str(trial) + '.in', "w+")
wr=""

rv1=truncnorm(0, 1, mu[0], 1)
rv2=truncnorm(0, 1, mu[1], 1)
for i in xrange(n*T):
  if i%n == 0:
    atom=0
    bit_1=1
    bit_2=1
  
  value_1=truncnorm.rvs(0,1, loc=mu[0], size=1)
  value_2=truncnorm.rvs(0,1, loc=mu[1], size=1)
  
  if i!=0:
    wr=wr[:-1]
    wr=wr+"\n"
    f.write(wr)
    wr=""
  
  if atom%n<n/2:
    if bit_1==1 and (atom+1)*(float(2)/n)>value_1:
      bit_1 = 0
  else:
    if bit_2==1 and (atom-n/2+1)*(float(2)/n)>value_2:
      bit_2=0

  for j in xrange(d+1):
    if atom%2<n/2:
      if j == 0:
        X[i][j] = bit_1*(atom+1)*(float(2)/n)
      else:
        X[i][j]=bit_1
    else:
      if j == 0:
        X[i][j]=bit_2*(atom-n/2+1)*(float(2)/n)
      else:
        X[i][j]=bit_2
    wr = wr + str(X[i][j]) + ","
  atom+=1
wr=wr[:-1]
wr=wr+"\n"
f.write(wr)

means=[[0 for i in xrange(d+1)] for j in xrange(n)]
for i in xrange(n*T):
  for j in xrange(d+1):
    means[i%n][j]+=X[i][j]/float(T)

np.savetxt('../DynamicPricing/Means_' + str(T) + '_' + str(trial) + '.in', means, delimiter=',')
