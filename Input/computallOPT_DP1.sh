#! /bin/bash

n=$1
folder=$2

for t in `seq 1000 1000 6000`;
do
  #t=12000
  B=$(($t/2))
  python computeOPT_DP1.py "$n" $t $B "$folder" > "$folder"/OPT_"$n"_"$t"_"$B"
done

for t in `seq 1000 1000 6000`;
do
  #t=12000
  B=$(($t/2))
  python computeOPT_UM.py "$n" $t $B "$folder" > "$folder"/OPTUM_"$n"_"$t"_"$B"
done

