import sys
import numpy as np
from numpy import array
import math
import cvxopt
from cvxopt import matrix,spmatrix, solvers
import random

n=int(sys.argv[1])
T=int(sys.argv[2])
B=int(sys.argv[3])
d=1
delta=0.01
K=2

means_f=open('../Input/Correlated/Means_' + str(n) + '_' + str(T) + '.in', 'r')

means=list()
for line in means_f:
  means.append(float(line.split("\n")[0]))

#Construct LP
W=list()
for i in xrange(n):
  W.append(-means[i])

b=[float(B)/T for i in xrange(d)] #Resources constraint

b.append(1) #matroid constraint
b.append(1) #matroid constraint

#Variable constraint
for i in xrange(n):
  b.append(1)

vals=list()
I=list()
J=list()

#LCB constraint
for i in xrange(d):
  for j in xrange(n):
    vals.append(means[j])
    I.append(i)
    J.append(j)

#Matroid constraint
for j in xrange(n/2):
  vals.append(1)
  I.append(d)
  J.append(j)

#Matroid constraint
for j in xrange(n/2, n):
  vals.append(1)
  I.append(d+1)
  J.append(j)

#Variable bounds constraint
for j in xrange(n):
  vals.append(1)
  I.append(d+j+2)
  J.append(j)

A=spmatrix(vals, I, J)
Wc=matrix(W)
bc=matrix(b)

#Solve LP
solvers.options['show_progress'] = False
sol=solvers.lp(Wc, A, bc) 
print -1*float(sol['primal objective'])
