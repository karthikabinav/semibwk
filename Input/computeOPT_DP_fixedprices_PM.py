import sys
import numpy as np
from numpy import array
import math
import cvxopt
from cvxopt import matrix,spmatrix, solvers
import random

n=int(sys.argv[1])
T=int(sys.argv[2])
B=int(sys.argv[3])
delta=float(sys.argv[4])
pid=sys.argv[5]

d=1
K=2

means_f=open('../Input/Means_DP_fixedprices_PM_' + str(n) + '_' + str(T)  + '_' + pid +  '.in', 'r')

means=[[0 for i in xrange(d+1)] for i in xrange(n)]
i=0
for line in means_f:
  vals=line.split(",")
  j=0
  for val in vals:
    means[i][j] = float(val.split("\n")[0])
    j+=1
  i+=1

#Construct LP
W=list()
for i in xrange(n):
  W.append(-means[i][0])

b=[float(B)/T for i in xrange(d)] #Resources constraint

b.append(1) #matroid constraint
b.append(1) #matroid constraint

#Variable constraint
for i in xrange(n):
  b.append(1)

vals=list()
I=list()
J=list()

#LCB constraint
for i in xrange(d):
  for j in xrange(n):
    vals.append(means[j][i+1])
    I.append(i)
    J.append(j)

#Matroid constraint
for j in xrange(n/2):
  vals.append(1)
  I.append(d)
  J.append(j)

for j in xrange(n/2, n):
  vals.append(1)
  I.append(d+1)
  J.append(j)


#Variable bounds constraint
for j in xrange(n):
  vals.append(1)
  I.append(d+j+2)
  J.append(j)

A=spmatrix(vals, I, J)
Wc=matrix(W)
bc=matrix(b)

#Solve LP
solvers.options['show_progress'] = False
sol=solvers.lp(Wc, A, bc) 
print -1*float(sol['primal objective'])
