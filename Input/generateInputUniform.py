import sys
import numpy as np
import random

n=int(sys.argv[1])
T=int(sys.argv[2])
d=1


x=np.random.random((n*T, d+1))
np.savetxt('../Input/Uniform/Feedback_' + str(n) + '_' + str(T) + '.in', x, delimiter=',')
