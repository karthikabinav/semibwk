import sys
import numpy as np
import random

n=int(sys.argv[1])
T=int(sys.argv[2])
d=1

num_products=2

x=np.random.random(n)

X=[[0 for i in xrange(d+1)] for j in xrange(n*T)]


atom=0
f=open('../Input/Correlated/Feedback_' + str(n) + '_' + str(T) + '.in', "w+")
wr=""
for i in xrange(n*T):
  if i%n == 0:
    atom=0
  if i!=0:
    wr=wr[:-1]
    wr=wr+"\n"
    f.write(wr)
    wr=""
  bit=np.random.binomial(1, x[atom])
  for j in xrange(d+1):
    if j == 0:
      X[i][j] = bit*np.random.binomial(1, x[atom])
    else:
      X[i][j]=np.random.binomial(1, x[atom])
    wr = wr + str(X[i][j]) + ","
  atom+=1
wr=wr[:-1]
wr=wr+"\n"
f.write(wr)

f.close()

means=[[0 for i in xrange(d+1)] for j in xrange(n)]
for i in xrange(n):
  for j in xrange(d+1):
    if j==0:
      means[i][j]=x[i]*x[i]
    else:
      means[i][j]=x[i]

np.savetxt('../Input/Correlated/Means_' + str(n) + '_' + str(T)  + '.in', x, delimiter=',')
