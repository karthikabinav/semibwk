import sys
import numpy as np
import random

n=int(sys.argv[1])
T=int(sys.argv[2])
mean_1=float(sys.argv[3])
mean_2=float(sys.argv[4])
folder=sys.argv[5]

d=1
num_products=2

valuation_mean=[mean_1, mean_2]

X=[[0 for i in xrange(d+1)] for j in xrange(n*T)]


atom=0
f=open('../Input/' + folder + '/Feedback_' + str(n) + '_' + str(T) + '.in', "w+")
wr=""

for i in xrange(n*T):
  if i%n == 0:
    atom=0
    bit_1=1
    bit_2=1
   
    valuation_1=-1
    valuation_2=-1

    while valuation_1<0 or valuation_1>1:
      valuation_1=np.random.normal(valuation_mean[0],1,1)
    while valuation_2<0 or valuation_1>1:
      valuation_2=np.random.normal(valuation_mean[1],1,1)

  if i!=0:
    wr=wr[:-1]
    wr=wr+"\n"
    f.write(wr)
    wr=""
  
  if atom%n<n/2:
    if bit_1==1 and atom*(float(2)/n)>=valuation_1:
      bit_1 = 0
  else:
    if bit_2==1 and (atom-n/2)*(float(2)/n)>=valuation_2:
      bit_2=0

  for j in xrange(d+1):
    if atom<n/2:
      if j == 0:
        X[i][j] = bit_1*atom*(float(2)/n)
      elif j!=0 and bit_1 == 0:
        X[i][j]=0.1
      elif j!=0:
        X[i][j]=(atom*(float(2)/n))
    else:
      if j == 0:
        X[i][j]=bit_2 *(atom-n/2)*(float(2)/n)
      elif j!=0 and bit_2==0:
        X[i][j]=0.1
      elif j!=0:
        X[i][j]=((atom-n/2)*(float(2)/n))

    wr = wr + str(X[i][j]) + ","
  atom+=1
wr=wr[:-1]
wr=wr+"\n"
f.write(wr)

f.close()

means=[[0 for i in xrange(d+1)] for j in xrange(n)]

for i in xrange(n*T):
  for j in xrange(d+1):
    means[i%n][j]+=X[i][j]/float(T)

np.savetxt('../Input/' + folder + '/Means_' + str(n) + '_' + str(T) + '.in', means, delimiter=',')
