import sys
import numpy as np
import random

n=int(sys.argv[1])
T=int(sys.argv[2])
pid=sys.argv[3]

d=1
num_products=2

prices_mean=[0.3, 0.7]

X=[[0 for i in xrange(d+1)] for j in xrange(n*T)]


atom=0
f=open('../Input/Feedback_DP_fixedprices_PM_' + str(n) + '_' + str(T) +  '_' + pid + '.in', "w+")
wr=""

for i in xrange(n*T):
  if i%n == 0:
    atom=0
    bit_1=1
    bit_2=1
   
    price_1=-1
    price_2=-1

    while price_1<0 or price_1>1:
      price_1=np.random.normal(prices_mean[0],1,1)
    while price_2<0 or price_1>1:
      price_2=np.random.normal(prices_mean[1],1,1)

  if i!=0:
    wr=wr[:-1]
    wr=wr+"\n"
    f.write(wr)
    wr=""
  
  if atom%n<n/2:
    if bit_1==1 and atom*(float(2)/n)>=price_1:
      bit_1 = 0
  else:
    if bit_2==1 and (atom-n/2)*(float(2)/n)>=price_2:
      bit_2=0

  for j in xrange(d+1):
    if atom%2<n/2:
      X[i][j] = bit_1
    else:
      X[i][j]=bit_2
    wr = wr + str(X[i][j]) + ","
  atom+=1
wr=wr[:-1]
wr=wr+"\n"
f.write(wr)

f.close()

means=[[0 for i in xrange(d+1)] for j in xrange(n)]

for i in xrange(n*T):
  for j in xrange(d+1):
    means[i%n][j]+=X[i][j]/float(T)

np.savetxt('../Input/Means_DP_fixedprices_PM_' + str(n) + '_' + str(T) +  '_' + pid + '.in', means, delimiter=',')
