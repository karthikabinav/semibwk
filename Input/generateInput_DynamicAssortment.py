import sys
import numpy as np
import random

n=int(sys.argv[1])
T=int(sys.argv[2])
folder=sys.argv[3]

d=1

fixed_prices=list()

for i in xrange(n):
  val=0.7
  while val>0.6:
    val=np.random.random()
  fixed_prices.append(val)

X=[[0 for i in xrange(d+1)] for j in xrange(n*T)]


atom=0
f=open('../Input/' + folder + '/Feedback_' + str(n) + '_' + str(T) + '.in', "w+")
wr=""

for i in xrange(n*T):
  if i%n == 0:
    atom=0

  if i!=0:
    wr=wr[:-1]
    wr=wr+"\n"
    f.write(wr)
    wr=""
  
  valuation=np.random.random()
  if valuation>fixed_prices[atom]:
    for j in xrange(d+1):
      if j == 0:
        X[i][j]=fixed_prices[atom]
      else:
        X[i][j]=1
      wr = wr + str(X[i][j]) + ","
  else:
    for j in xrange(d+1):
      X[i][j]=valuation
      wr = wr + str(X[i][j]) + ","
  atom+=1
wr=wr[:-1]
wr=wr+"\n"
f.write(wr)

f.close()

means=[[0 for i in xrange(d+1)] for j in xrange(n)]

for i in xrange(n*T):
  for j in xrange(d+1):
    means[i%n][j]+=X[i][j]/float(T)

np.savetxt('../Input/' + folder + '/Means_' + str(n) + '_' + str(T) + '.in', means, delimiter=',')
