import sys
import numpy as np
import random

n=int(sys.argv[1])
T=int(sys.argv[2])

d=1

x=[0 for i in xrange(n)]
xfull=[[0 for i in xrange(d+1)] for j in xrange(n)]

for i in xrange (n):
  if i%5 >= 3:
    x[i]=1
  for j in xrange(d+1):
    xfull[i][j]=x[i]

X=[[0 for i in xrange(d+1)] for j in xrange(n*T)]


atom=0
f=open('../Input/Deterministic/Feedback_' + str(n) + '_' + str(T) + '.in', "w+")
wr=""
for i in xrange(n*T):
  if i%n == 0:
    atom=0
  if i!=0:
    wr=wr[:-1]
    wr=wr+"\n"
    f.write(wr)
    wr=""
  bit=np.random.binomial(1, x[atom])
  for j in xrange(d+1):
    X[i][j] = bit
    wr = wr + str(X[i][j]) + ","
  atom+=1
wr=wr[:-1]
wr=wr+"\n"
f.write(wr)

f.close()
np.savetxt('../Input/Deterministic/Means_' + str(n) + '_' + str(T)  + '.in', xfull, delimiter=',')
