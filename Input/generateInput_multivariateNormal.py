import sys
import numpy as np
import random
from scipy import random, linalg

n=int(sys.argv[1])
T=int(sys.argv[2])
folder=sys.argv[3]

d=1

X=[[0 for i in xrange(d+1)] for j in xrange(n*T)]

means=[[0 for i in xrange(d+1)] for j in xrange(n)]

expandedMeans=[0 for i in xrange(n*(d+1))]
expandedCovariance=[ [0 for i in xrange(len(expandedMeans))] for j in xrange(len(expandedMeans))]

for i in xrange(n):
  for j in xrange(d+1):
    means[i][j]=np.random.random()
    expandedMeans[i*(d+1)+j]=means[i][j]

matrixSize=len(expandedMeans)
A=random.rand(matrixSize,matrixSize)
expandedCovariance=np.dot(A,A.transpose())

atom=0
f=open('../Input/' + folder + '/Feedback_' + str(n) + '_' + str(T) + '.in', "w+")
wr=""

sums=[0 for i in xrange(d+1)]
for i in xrange(n*T):
  if i%n == 0:
    atom=0
    y = np.random.multivariate_normal(expandedMeans, expandedCovariance, size=n*(d+1))
    y=abs(y[0])/(abs(y[0])+abs(y[1]))
  if i!=0:
    wr=wr[:-1]
    wr=wr+"\n"
    f.write(wr)
    wr=""

  for j in xrange(d+1):
    X[i][j] = min(1, y[atom*(d+1)+j])
    wr = wr + str(X[i][j]) + ","
  atom+=1
wr=wr[:-1]
wr=wr+"\n"
f.write(wr)

f.close()

means=[[0 for i in xrange(d+1)] for j in xrange(n)]

for i in xrange(n*T):
  for j in xrange(d+1):
    means[i%n][j]+=X[i][j]/float(T)

np.savetxt('../Input/' + folder + '/Means_' + str(n) + '_' + str(T) + '.in', means, delimiter=',')
