import sys
import numpy as np
import random

n=int(sys.argv[1])
T=int(sys.argv[2])

d=1

x=np.random.rand(n, d+1)

X=[[0 for i in xrange(d+1)] for j in xrange(n*T)]


atom=0
f=open('../Input/Nonuniform/Feedback_' + str(n) + '_' + str(T) +'.in', "w+")
wr=""
for i in xrange(n*T):
  if i%n == 0:
    atom=0
  if i!=0:
    wr=wr[:-1]
    wr=wr+"\n"
    f.write(wr)
    wr=""
  for j in xrange(d+1):
    X[i][j] = np.random.binomial(1, x[atom][j])
    wr = wr + str(X[i][j]) + ","
  atom+=1
wr=wr[:-1]
wr=wr+"\n"
f.write(wr)

f.close()
np.savetxt('../Input/Nonuniform/Means_' + str(n) + '_' + str(T) + '.in', x, delimiter=',')
