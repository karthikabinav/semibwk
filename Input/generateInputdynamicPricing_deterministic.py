import sys
import numpy as np
import random

n=int(sys.argv[1])
T=int(sys.argv[2])
d=int(sys.argv[3])
pid=sys.argv[4]

#num_products=n/5

#p=np.random.random(num_products)*10

x=[0 for i in xrange(n)]

for i in xrange (n):
  if i%5 >= 3:
    x[i]=1

X=[[0 for i in xrange(d+1)] for j in xrange(n*T)]


atom=0
f=open('../Input/Feedback_deterministic_' + str(n) + '_' + str(T) + '_' + str(d) + '_' + pid + '.in', "w+")
wr=""
for i in xrange(n*T):
  if i%n == 0:
    atom=0
  if i!=0:
    wr=wr[:-1]
    wr=wr+"\n"
    f.write(wr)
    wr=""
  bit=np.random.binomial(1, x[atom])
  for j in xrange(d+1):
    X[i][j] = bit
    wr = wr + str(X[i][j]) + ","
  atom+=1
wr=wr[:-1]
wr=wr+"\n"
f.write(wr)

f.close()
np.savetxt('../Input/Means_deterministic_' + str(n) + '_' + str(T) + '_' + str(d) + '_' + pid + '.in', x, delimiter=',')
