#!/bin/bash

# Lines that begin with #SBATCH specify commands to be used by SLURM for scheduling

#SBATCH --job-name=OPT                             # sets the job name
#SBATCH --output OPT.out.%j                              # indicates a file to redirect STDOUT to; %j is the jobid 
#SBATCH --error OPT.out.%j                               # indicates a file to redirect STDERR to; %j is the jobid
#SBATCH --time=08:59:59                                         # how long you think your job will take to complete; format=hh:mm:ss
#SBATCH --partition dpart
#SBATCH --gres=gpu:0
#SBATCH --nodes=4                                               # number of nodes to allocate for your job
#SBATCH --ntasks=28                                              # request 4 cpu cores be reserved for your node total
#SBATCH --ntasks-per-node=7                                    # request 2 cpu cores be reserved per node
#SBATCH --mem=50gb                                               # memory required by job; if unit is not specified MB will be assumed

source ../project_files/bin/activate
module load matlab/2016b

declare -a arr=(6 26)
for i in "${arr[@]}";
do
  (t=6000;B=1000;echo "Started $i $t $B";srun --exclusive -N1 -n1 python computeOPT_DP_PM.py $i $t $B 0.01 DynamicAssortment1 > ./DynamicAssortment1/OPT_"$i"_"$t"_"$B"; echo "Done $i $t $B") &
  (t=6000;B=3000;echo "Started $i $t $B";srun --exclusive -N1 -n1 python computeOPT_DP_PM.py $i $t $B 0.01 DynamicAssortment1 > ./DynamicAssortment1/OPT_"$i"_"$t"_"$B"; echo "Done $i $t $B") &
  (t=6000;B=5000;echo "Started $i $t $B";srun --exclusive -N1 -n1 python computeOPT_DP_PM.py $i $t $B 0.01 DynamicAssortment1 > ./DynamicAssortment1/OPT_"$i"_"$t"_"$B"; echo "Done $i $t $B") &
done
for i in "${arr[@]}";
do
  (t=6000;B=1000;echo "Started $i $t $B";srun --exclusive -N1 -n1 python computeOPT_DP_PM.py $i $t $B 0.01 DynamicAssortment2 > ./DynamicAssortment2/OPT_"$i"_"$t"_"$B"; echo "Done $i $t $B") &
  (t=6000;B=3000;echo "Started $i $t $B";srun --exclusive -N1 -n1 python computeOPT_DP_PM.py $i $t $B 0.01 DynamicAssortment2 > ./DynamicAssortment2/OPT_"$i"_"$t"_"$B"; echo "Done $i $t $B") &
  (t=6000;B=5000;echo "Started $i $t $B";srun --exclusive -N1 -n1 python computeOPT_DP_PM.py $i $t $B 0.01 DynamicAssortment2  > ./DynamicAssortment2/OPT_"$i"_"$t"_"$B" ; echo "Done $i $t $B") &
done
for i in "${arr[@]}";
do
  (t=6000;B=1000;echo "Started $i $t $B";srun --exclusive -N1 -n1 python computeOPT_DP_PM.py $i $t $B 0.01 DynamicAssortment3 > ./DynamicAssortment3/OPT_"$i"_"$t"_"$B"; echo "Done $i $t $B") &
  (t=6000;B=3000;echo "Started $i $t $B";srun --exclusive -N1 -n1 python computeOPT_DP_PM.py $i $t $B 0.01 DynamicAssortment3 > ./DynamicAssortment3/OPT_"$i"_"$t"_"$B"; echo "Done $i $t $B") &
  (t=6000;B=5000;echo "Started $i $t $B";srun --exclusive -N1 -n1 python computeOPT_DP_PM.py $i $t $B 0.01 DynamicAssortment3 > ./DynamicAssortment3/OPT_"$i"_"$t"_"$B"; echo "Done $i $t $B") &
done

for i in "${arr[@]}";
do
  (t=6000;B=1000;echo "Started $i $t $B";srun --exclusive -N1 -n1 python computeOPT_DP_PM.py $i $t $B 0.01 DynamicAssortment4 > ./DynamicAssortment4/OPT_"$i"_"$t"_"$B"; echo "Done $i $t $B") &
  (t=6000;B=3000;echo "Started $i $t $B";srun --exclusive -N1 -n1 python computeOPT_DP_PM.py $i $t $B 0.01 DynamicAssortment4 > ./DynamicAssortment4/OPT_"$i"_"$t"_"$B"; echo "Done $i $t $B") &
  (t=6000;B=5000;echo "Started $i $t $B";srun --exclusive -N1 -n1 python computeOPT_DP_PM.py $i $t $B 0.01 DynamicAssortment4 > ./DynamicAssortment4/OPT_"$i"_"$t"_"$B"; echo "Done $i $t $B") &
done
for i in "${arr[@]}";
do
  (t=6000;B=1000;echo "Started $i $t $B";srun --exclusive -N1 -n1 python computeOPT.py $i $t $B > ./Correlated/OPT_"$i"_"$t"_"$B"; echo "Done $i $t $B") &
  (t=6000;B=3000;echo "Started $i $t $B";srun --exclusive -N1 -n1 python computeOPT.py $i $t $B > ./Correlated/OPT_"$i"_"$t"_"$B"; echo "Done $i $t $B") &
  (t=6000;B=5000;echo "Started $i $t $B";srun --exclusive -N1 -n1 python computeOPT.py $i $t $B > ./Correlated/OPT_"$i"_"$t"_"$B"; echo "Done $i $t $B") &
done
wait
