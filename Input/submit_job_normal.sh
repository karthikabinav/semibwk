#!/bin/bash

# Lines that begin with #SBATCH specify commands to be used by SLURM for scheduling

#SBATCH --job-name=normalinputs                              # sets the job name
#SBATCH --output normalinputs.out.%j                              # indicates a file to redirect STDOUT to; %j is the jobid 
#SBATCH --error normalinputs.out.%j                               # indicates a file to redirect STDERR to; %j is the jobid
#SBATCH --time=08:59:59                                         # how long you think your job will take to complete; format=hh:mm:ss
#SBATCH --partition dpart
#SBATCH --gres=gpu:0
#SBATCH --nodes=4                                               # number of nodes to allocate for your job
#SBATCH --ntasks=28                                              # request 4 cpu cores be reserved for your node total
#SBATCH --ntasks-per-node=7                                    # request 2 cpu cores be reserved per node
#SBATCH --mem=50gb                                               # memory required by job; if unit is not specified MB will be assumed

source ../project_files/bin/activate
module load matlab/2016b

for i in `seq 4 6 35`;
do
  (t=1200;B=1000;echo "Started $i $t $B";srun --exclusive -N1 -n1 python generateInput_multivariateNormal.py $i $t Normal; echo "Done $i $t $B") &
  (t=1200;B=2500;echo "Started $i $t $B";srun --exclusive -N1 -n1 python generateInput_multivariateNormal.py $i $t Normal; echo "Done $i $t $B") &
  (t=4700;B=8000;echo "Started $i $t $B";srun --exclusive -N1 -n1 python generateInput_multivariateNormal.py $i $t Normal; echo "Done $i $t $B") &
done

wait
