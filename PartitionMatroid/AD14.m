function main=AD14(inFile, outFile, n, T, d, K, B, OPT)
cd ~/cvx
cvx_setup

cd ~/semiBwK/Simulations/UniformMatroid

f=csvread(inFile);

%n=10;
%T=1200;
%d=5;
delta=0.01;
alpha=log(n*T*d/delta);
%tau=sqrt(n*T*alpha) + alpha*n+1;
%B=round(tau*2);

%B=1300;
%K=2;
OPT=T*OPT;

sets = {1:n/2, n/2+1:n};
[x y] = ndgrid(sets{:});
cartProd = [x(:) y(:)];
superArms=cartProd;

fileID = fopen(outFile,'a+');


n_sa=length(superArms);
K_sa=1;
B_sa=B/2;

feedback=reshape(f, [T, n, d+1]);

Xt=zeros(n,n_sa);
for i=1:size(superArms,1)
    for j=1:size(superArms,2)
        Xt(superArms(i,j),i)=1;
    end
end

%Compute Z
T0=n_sa*sqrt(T);

%{
cumSum=zeros(n);
prevX=zeros(1,n);

cumMuSum=zeros(n_sa);
Mut=zeros(n_sa, T0);

for t=1:T0
    Mt=eye(n) + cumSum + prevX*prevX';
    Mtinv=inv(Mt);
    
    H=Xt'*Mtinv*Xt;
    
    [p, val]=quadprog(H,[],ones(1,n_sa), 1 ,[],[],zeros(n_sa),ones(n_sa));
   
    
    Xtp = Xt*p;
    a_t=myfun(p',1:n_sa);
    
    %Construct estimates mu_hat and W_hat here.
    
    
    
    cumSum=cumSum+Xtp*Xtp';
    prevX=Xtp;
    
    %Compute OPT_hat here
end
%}

%T=T-T0;
Z=OPT/B_sa + 1;
%B_sa=B_sa-T0;
epsilon=sqrt(log(d+1)/T);


%Implement the actual algorithm 
prevX=zeros(1, n);
cumSum=eye(n);

mu_hat=zeros(n);
cumSumMu=zeros(1, n);
prevMu=zeros(1, n);

W_hat=zeros(n, d);
cumSumW=zeros(d, n);
prevW=zeros(d,n);

theta_t=ones(d,1);
alpha_t=ones(d,1);

consumed=zeros(d,1);
reward=0;

mu_tilde=zeros(n,n_sa);
W_tilde=zeros(n_sa, n, d);

fact=1.2
prevDet=0

for t=1:T
    t
    Mt=cumSum + prevX*prevX';
    Mtinv=inv(Mt);
    
    cumSum = cumSum + prevX*prevX';

    %For each arm compute mu_tilde
    for a=1:n_sa
        mu_hat=Mtinv*(cumSumMu + prevMu)';
        Xta = Xt(:,a)';
        [ Qsqrt, p, S  ] = chol( sparse(Mt) );
        Qsqrt = Qsqrt * S;
        cvx_begin quiet
            variable mu1(n)
            maximize ( Xta * mu1 )
            subject to
                norm(Qsqrt * (mu1-mu_hat))<=sqrt(n*log((d + n*t*d)/delta)) + sqrt(n)
                %quad_form(mu1-mu_hat, Mt)<=sqrt(n*log((d + n*t*d)/delta)) + sqrt(n)
        cvx_end
        mu_tilde(:,a)=mu1;
    end 
    cumSumMu = cumSumMu + prevMu;
    
    
    %For each arm and resource compute W_tilde
    for a=1:n_sa
        
        for res=1:d
            W_hat(:,res)=Mtinv*(cumSumW(res, :) + prevW(res, :))';
        end
        Xta = Xt(:,a)';
        
        cvx_begin quiet
            variable W(n, d)
            minimize ( Xta * W * theta_t )
            subject to
                for j=1:d
                   norm(Qsqrt * (W(:,j)-W_hat(:,j)))<=sqrt(n*log((d + n*t*d)/delta)) + sqrt(n)
                   %quad_form(W(:,j)-W_hat(:,j), Mt)<=sqrt(n*log((d + n*t*d)/delta)) + sqrt(n)
                end
        cvx_end
        W_tilde(a,:,:)=full(W);
        
    end
    cumSumW = cumSumW+prevW;
    
    
    a_t=0;
    maxi=-1;
    %Play the arm with maximumg value
    for a=1:n_sa
        Xta = Xt(:,a)';
        W_tildea=reshape(W_tilde(a, :, :), [n, d]);

        
       Ca=Xta * (mu_tilde(:,a) - (Z * (W_tildea * theta_t)));
       if Ca>maxi
           maxi=Ca;
           a_t=a;
       end
    end
    
    feedbackt=reshape(feedback(t,:,:)/2,[n, d+1]);
    feedback_sa=feedbackt'*Xt;
    r_t=feedback_sa(1,a_t);
    reward= reward + r_t;
    v_t=feedback_sa(2:d+1,a_t);
    consumed = consumed + v_t;
    
    br=false;
    for j=1:d
        if consumed(j) > B_sa
            br=true;
        end
    end
    
    if br
        break
    end
    
    %Compute mu_hat
    prevMu=(Xt(:,a_t)*r_t)';
    
    %Compute W_hat
    prevW=v_t*Xt(:,a_t)';
    
    prevX=Xt(:,a_t)';
    
    %Online Learning Update
    g_t=theta_t.*(v_t-(B_sa/T)*ones(d,1));
    total=0;
    for res=1:d
        mul=1;
        if(g_t(res,:) > 0)
            mul=(1+epsilon)^g_t(res,:);
        else
            mul=(1-epsilon)^(-g_t(res,:));
        end
       alpha_t(res,:)=alpha_t(res,:) * mul;
       total= total + alpha_t(res,:);
    end
    for res=1:d
       theta_t(res,:)=alpha_t(res,:)/(1+total); 
    end
    if t>200
      fprintf(fileID,'%0.2f\n',2*reward/T);
    end
end


end

function F = myfun(P,X) 
p = cumsum([0; P(1:end-1).'; 1+1e3*eps]); 
[a a] = histc(rand,p); 
F = X(a);
end
