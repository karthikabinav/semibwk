import sys
import numpy as np
from numpy import array
import math
import cvxopt
from cvxopt import matrix,spmatrix, solvers
import random
import itertools
from itertools import islice
import time

def getSuperArms():
  return list(itertools.product(xrange(n/2), xrange(n/2, n)))

def rad(X, N):
  A= math.sqrt(float(alpha*X)/float(N))
  B=float(alpha)/float(N)
  return A+B

def out_of_resources():
  for i in xrange(len(consumption)):
    if consumption[i]>B_sa:
      return True
  return False

def indep_sample(vec):
  r=random.random()
  ret = list()
  cur=0
  for i in xrange(len(vec)):
    if vec[i]+cur>=r:
      ret.append(i)
      return ret
    cur+=vec[i]
  ret.append(n_sa-1)
  return ret

def getAction():
  #Construct LP
  maxi=-1
  played=-1
  for X in xrange(n_sa):
    if ECost[X]==0:
      return X
    if float(UCB[X][0])/float(ECost[X]) > maxi:
      maxi=float(UCB[X][0])/float(ECost[X])
      played=X
  return played

def getFeedback(t):
  global feedback_sa
  feedback_sa = [[0 for k in xrange(d+1)] for j in xrange(n_sa)]
  for a in xrange(n_sa):
    for i in xrange(d+1):
      for arms in superArms[a]:
        feedback_sa[a][i] += feedback[t][arms][i]/float(K)
  
n=int(sys.argv[1])
T=int(sys.argv[2])
d=int(sys.argv[3])
B=int(sys.argv[4])
delta=float(sys.argv[5])
K=int(sys.argv[6])
f_feedback=sys.argv[7]


filename=f_feedback

superArms=getSuperArms()

n_sa=len(superArms)
K_sa=1
B_sa=B/K


feedback = [[[0 for k in xrange(d+1)] for j in xrange(n)] for i in xrange(T)]
feedback_sa = [[0 for k in xrange(d+1)] for j in xrange(n_sa)]

f=open(filename, "r")
arm=0
timel=0

for line in f:
  vals=line.split(",")
  if arm==n:
    arm=0
    timel+=1
  for i in xrange(len(vals)):
    feedback[timel][arm][i]=float(vals[i].split("\n")[0])
  arm+=1


alpha=np.log((n_sa*d*T)/delta)
#Exploration phase
N = [0 for j in xrange(n_sa)]
average=[[float(0) for k in xrange(d+1)] for j in xrange(n_sa)]
UCB = [[float(0) for k in xrange(d+1)] for j in xrange(n_sa)]
LCB = [[float(0) for k in xrange(d+1)] for j in xrange(n_sa)]
V = [1 for j in xrange(d)]

reward=0
consumption=[float(0) for k in xrange(d)]

for t in xrange(n_sa):
  if out_of_resources():
    print "MAYDAY"
    break
  getFeedback(t)
  N[t]+=1
  observed=feedback_sa[t]
  for l in xrange(len(observed)):
    average[t][l]=(average[t][l]*(N[t]-1) + observed[l])/N[t]
    UCB[t][l]=min(1, average[t][l] + rad(average[t][l], N[t]))
    LCB[t][l]=max(0, average[t][l] - rad(average[t][l], N[t]))
    if l==0:
      reward+=observed[0]
    else:
      consumption[l-1]+=observed[l]
epsilon=math.sqrt(np.log(d+1)/float(B_sa))
ECost=[0 for i in xrange(n_sa)]

for x in xrange(n_sa):
  ECost[x]=sum(i[0] * i[1] for i in zip(LCB[x][1:], V))

last=time.clock()
steps=1
tries=0
for t in xrange(n_sa, T):
  if t%100 == 99:
    print (time.clock()-last)/steps
    steps=1
    tries=1
    if tries>50:
      break
    last=time.clock()

  #print (K*reward)
  if out_of_resources():
    break
  played=getAction()
  getFeedback(t)
  #print played 
  #Update the vectors
  X=played
  N[X]+=1
  observed=feedback_sa[X]
  
  for l in xrange(len(observed)):
    if l != 0:
      V[l-1]=V[l-1]*((1+epsilon)**LCB[X][l])
    average[X][l]=(average[X][l]*(N[X]-1) + observed[l])/N[X]
    UCB[X][l]=min(1, average[X][l] + rad(average[X][l], N[X]) )
    LCB[X][l]=max(0, average[X][l] - rad(average[X][l], N[X]) )
    if l==0:
      reward+=observed[0]
    else:
      consumption[l-1]+=observed[l]
    
  #update the values for all arms
  for x in xrange(n_sa):
    ECost[x]=sum(i[0] * i[1] for i in zip(LCB[x][1:], V))
    #print UCB
    #print LCB
  steps+=1
#for tt in xrange(t,T):
#  print (K*reward)
