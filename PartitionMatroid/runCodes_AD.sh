#!/bin/bash

#Calls to various codes. Uncomment and use 
n=$1
T=$2
B=$3

folder=$4

d=1
K=2
delta=0.01

append=$$

#Generate Random Inputs
#filename=Feedback_"$n"_"$T".in
filename=Feedback_"$T"_1.in

full_path=../Input/$folder/$filename

i=1
matlab -nodesktop -nosplash -nojvm -r "AD14_sampling_ac('$full_path','../Output/AD16Sampling_"$append"_"$folder"_"$i"',$n, $T, $d, $K, $B, $(head -n 1 ../Input/$folder/OPT_"$T"_1));quit"

mv ../Output/AD16Sampling_"$append"_"$folder"_"$i" ../Output/Output_AD16_"$folder"_"$n"_"$B".out

#python computeAvg.py ../Output/AD16_"$append"_"$folder" ../Output/Output_AD16_"$folder"_"$n"_"$B".out

#for i in `seq 1 10`;
#do
#  rm ../Output/AD16_"$append"_"$folder"_"$i"
#done



