#!/bin/bash

#Calls to various codes. Uncomment and use 
n=$1
T=$2
B=$3

folder=$4

d=1
K=2
delta=0.01

append=$$

#Generate Random Inputs
#filename=Feedback_"$n"_"$T".in
filename=Feedback_"$T"_1.in

full_path=../Input/$folder/$filename

for i in `seq 1 10`;
do
python semiUCBBwK_PM_ac.py $n $T $d $B $delta $K $full_path >> ../Output/semiUCBBwK_"$append"_"$folder"_"$i"
done

python computeAvg.py ../Output/semiUCBBwK_"$append"_"$folder" ../Output/Output_semiUCBBwK_"$folder"_"$n"_"$B".out

for i in `seq 1 10`;
do
  rm ../Output/semiUCBBwK_"$append"_"$folder"_"$i"
done

