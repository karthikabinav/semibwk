#!/bin/bash

#Calls to various codes. Uncomment and use 



B=3000

folder='DynamicAssortment3'

d=1
K=2
delta=0.01

append=$$

#Generate Random Inputs


#for i in `seq 1 10`;
#do
i=10; T=1000;
filename=Feedback_"$i"_"$T".in;
full_path=../Input/$folder/$filename
python semiUCBBwK_PM.py 10 1000 $d $B $delta $K $full_path >> ../Output/OutputTimes_"$folder"_"$i"
i=20;T=1000
filename=Feedback_"$i"_"$T".in;
full_path=../Input/$folder/$filename

python semiUCBBwK_PM.py $i 1000 $d $B $delta $K $full_path >> ../Output/OutputTimes_"$folder"_"$i"
i=30;T=1000
filename=Feedback_"$i"_"$T".in;
full_path=../Input/$folder/$filename

python semiUCBBwK_PM.py $i 1000 $d $B $delta $K $full_path >> ../Output/OutputTimes_"$folder"_"$i"
i=40;T=2000
filename=Feedback_"$i"_"$T".in;
full_path=../Input/$folder/$filename

python semiUCBBwK_PM.py $i 2000 $d $B $delta $K $full_path >> ../Output/OutputTimes_"$folder"_"$i"
i=50;T=3000
filename=Feedback_"$i"_"$T".in;
full_path=../Input/$folder/$filename

python semiUCBBwK_PM.py $i 3000 $d $B $delta $K $full_path >> ../Output/OutputTimes_"$folder"_"$i"
i=75;T=6000
filename=Feedback_"$i"_"$T".in;
full_path=../Input/$folder/$filename

python semiUCBBwK_PM.py $i 6000 $d $B $delta $K $full_path >> ../Output/OutputTimes_"$folder"_"$i"
i=100;T=6000
filename=Feedback_"$i"_"$T".in;
full_path=../Input/$folder/$filename

python semiUCBBwK_PM.py $i 6000 $d $B $delta $K $full_path >> ../Output/OutputTimes_"$folder"_"$i"


#done

#python computeAvg.py ../Output/semiUCBBwK_"$append"_"$folder" ../Output/Output_semiUCBBwK_"$folder"_"$n"_"$B".out

#for i in `seq 1 10`;
#do
#  rm ../Output/semiUCBBwK_"$append"_"$folder"_"$i"
#done

