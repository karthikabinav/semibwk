import sys
import numpy as np
from numpy import array
import math
import cvxopt
from cvxopt import matrix,spmatrix, solvers
import random
import time


def rad(X, N):
  A= math.sqrt(float(alpha*X)/float(N))
  B=float(alpha)/float(N)
  return A+B

def out_of_resources():
  for i in xrange(len(consumption)):
    if consumption[i]>Borig:
      return True
  return False

def rand_round_uniform_matroid(vec, card):
  pos1=0
  pos2=1
  
  times=dict()
  n_len=len(vec)
  for i in xrange(n_len):
    times[i]=0
  
  sumi=0
  for i in vec:
    assert (0-0.01<=i<=1+0.01)
    sumi+=i
  assert(sumi<=1.01)
  for i in xrange(100):
    while pos1<n_len and pos2<n_len:
      if vec[pos1] == 1 or vec[pos1] == 0:
        temp=pos2
        pos1=pos2
        pos2=temp+1
        continue
      if vec[pos2] == 1 or vec[pos2] == 0:
        pos2+=1
        continue
      
      a=vec[pos1]
      b=vec[pos2]
      alpha = min(1-a,b)
      beta=min(a, 1-b)

      r=random.random()
      if r<=beta/(alpha+beta):
        vec[pos1]+=alpha
        vec[pos2]-=alpha
      else:
        vec[pos1]-=beta
        vec[pos2]+=beta
    
    ret=-1
    for i in xrange(len(vec)):
      if vec[i]==1:
        ret = i
        break
    if ret!=-1:
      times[ret]+=1
  maxi=times[0]
  ret=0
  for key in times:
    if times[key]>maxi:
      maxi=times[key]
      ret=key
  return ret

def rand_round_partition_matroid(vec):
  actions=list()
  #Get the price for first product
  actions.append(rand_round_uniform_matroid(vec[:n/2], 1))
  #Get the price for second product
  actions.append(rand_round_uniform_matroid(vec[n/2:], 1))
  return actions

def getAction():
  #Construct LP
  W=list()
  for i in xrange(n):
    W.append(-1 * float(UCB[i][0]))
  b=[B/float(T) for i in xrange(d)] #Resources constraint
  
  b.append(1) #matroid constraint for first product
  b.append(1) #matroid constraint for second product
  
  #Variable constraint
  for i in xrange(n):
    b.append(1)
  
  for i in xrange(n):
    b.append(0)

  vals=list()
  I=list()
  J=list()
  
  #LCB constraint
  for i in xrange(d):
    for j in xrange(n):
      vals.append(LCB[j][i+1])
      I.append(i)
      J.append(j)

  #Matroid constraint for the first product
  for j in xrange(n/2):
    vals.append(1)
    I.append(d)
    J.append(j)
  
  #Matroid Constraint for the second product
  for j in xrange(n/2, n):
    vals.append(1)
    I.append(d+1)
    J.append(j)

  #Variable bounds constraint
  for j in xrange(n):
    vals.append(1)
    I.append(d+j+2)
    J.append(j)
  
  for j in xrange(n):
    vals.append(-1)
    I.append(d+n+2+j)
    J.append(j)

  A=spmatrix(vals, I, J)
  Wc=matrix(W)
  bc=matrix(b)
  
  #Solve LP
  solvers.options['show_progress'] = False
  sol=solvers.lp(Wc, A, bc) 
  vec = sol['x']
  #Randomized Rounding
  return rand_round_partition_matroid(vec)

n=int(sys.argv[1])
T=int(sys.argv[2])
d=int(sys.argv[3])
B=int(sys.argv[4])
delta=float(sys.argv[5])
K=int(sys.argv[6])
f_feedback=sys.argv[7]

#alpha=10*np.log((n*d*T)/delta)
alpha=5
#epsilon=0.9
Borig=B
#B=B(1-epsilon)


f=open(f_feedback, "r")

feedback = [[[0 for k in xrange(d+1)] for j in xrange(n)] for i in xrange(T)]

arm=0
timel=0

for line in f:
  vals=line.split(",")
  if arm==n:
    arm=0
    timel+=1
  for i in xrange(len(vals)):
    feedback[timel][arm][i]=float(vals[i].split("\n")[0])
  arm+=1

#Exploration phase
N = [0 for j in xrange(n)]
average=[[float(0) for k in xrange(d+1)] for j in xrange(n)]
UCB = [[float(0) for k in xrange(d+1)] for j in xrange(n)]
LCB = [[float(0) for k in xrange(d+1)] for j in xrange(n)]

reward=0
consumption=[float(0) for k in xrange(d)]

for t in xrange(n):
  if out_of_resources():
    break

  N[t]+=1
  observed=feedback[t][t]
  for l in xrange(len(observed)):
    average[t][l]=(average[t][l]*(N[t]-1) + observed[l])/N[t]
    UCB[t][l]=min(1, average[t][l] + rad(average[t][l], N[t]) )
    LCB[t][l]=max(0, average[t][l] - rad(average[t][l], N[t]) )
    if l==0:
      reward+=observed[0]
    else:
      consumption[l-1]+=observed[l]

last=time.clock()
steps=1
tries=0
for t in xrange(n, T):
  if t%100 == 99:
    print (time.clock()-last)/steps
    steps=1
    tries=1
    if tries>50:
      break
    last=time.clock()
  #print reward
  if out_of_resources():
    break
  played=getAction()
  #print played 
  #Update the vectors
  assert len(played)<=K
  for x in played:
    if x==-1:
      continue
    N[x]+=1
    observed=feedback[t][x]
  
    for l in xrange(len(observed)):
      average[x][l]=(average[x][l]*(N[x]-1) + observed[l])/N[x]
      UCB[x][l]=min(1, average[x][l] + rad(average[x][l], N[x]) )
      LCB[x][l]=max(0, average[x][l] - rad(average[x][l], N[x]) )
      if l==0:
        reward+=observed[0]
      else:
        consumption[l-1]+=observed[l]
  steps+=1  
    #print UCB
    #print LCB
#for tt in xrange(t, T):
#  print reward
