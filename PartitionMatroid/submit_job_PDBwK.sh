#!/bin/bash

# Lines that begin with #SBATCH specify commands to be used by SLURM for scheduling

#SBATCH --job-name=PDBwK                              # sets the job name
#SBATCH --output PDBwK.out.%j                              # indicates a file to redirect STDOUT to; %j is the jobid 
#SBATCH --error PDBwK.out.%j                               # indicates a file to redirect STDERR to; %j is the jobid
#SBATCH --time=08:59:59                                         # how long you think your job will take to complete; format=hh:mm:ss
#SBATCH --partition dpart
#SBATCH --gres=gpu:0
#SBATCH --nodes=4                                               # number of nodes to allocate for your job
#SBATCH --ntasks=28                                              # request 4 cpu cores be reserved for your node total
#SBATCH --ntasks-per-node=7                                    # request 2 cpu cores be reserved per node
#SBATCH --mem=50gb                                               # memory required by job; if unit is not specified PDBwK will be assumed

source ../project_files/bin/activate
module load matlab/2016b
K=2
declare -a arr=(6 26)
#for i in "${arr[@]}";
#do
#for tt in `seq 1000 1000 6000`;
#do
#  (t=$tt;B=$(($t/2));echo "Started $i $t $B";srun --exclusive -N1 -n1 bash runCodes_PDBwK.sh $i $t $B DA1 ; echo "Done $i $t $B") &
#done
#done
#for i in "${arr[@]}";
#do
#for tt in `seq 1000 1000 6000`;
#do
#  (t=$tt;B=$(($t/2));echo "Started $i $t $B";srun --exclusive -N1 -n1 bash runCodes_PDBwK.sh $i $t $B DA2 ; echo "Done $i $t $B") &
#done
#done
i=26
for tt in `seq 1000 1000 6000`;
do
  (t=$tt;B=$(($t/2));echo "Started $i $t $B";srun --exclusive -N1 -n1 bash runCodes_PDBwK.sh $i $t $B DynamicPricing ; echo "Done $i $t $B") &
done

wait
