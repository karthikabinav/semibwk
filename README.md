# SemiBwK

This contains the code used for simulations in the paper "Combinatorial Semi-Bandits with Knapsacks" that appeared in AIStats 2018. I will re-visit this and make this code more user-friendly.
In the meantime if you wanted to use parts of this code and weren't able to get it running, feel free to contact Karthik Abinav Sankararaman (kabinav@cs.umd.edu).

If you use parts of this code, please cite our paper using the following bibtex.

@inproceedings{SS2018,
  title={Combinatorial Semi-Bandits with Knapsacks},
  author={Sankararaman, Karthik Abinav and Slivkins, Aleksandrs},
  booktitle={International Conference on Artificial Intelligence and Statistics},
  pages={1760--1770},
  year={2018}
}



