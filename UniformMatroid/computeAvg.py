import sys

inp=sys.argv[1]
out=sys.argv[2]

vals=list()

first=True
for i in xrange(1,11):
  f=open(inp + "_" + str(i), "r")
  cnt=0
  for line in f:
    if first:
      vals.append(float(line.split("\n")[0]))
    else:
      vals[cnt]+=float(line.split("\n")[0])
      cnt+=1
  first=False

vals=[x / float(10) for x in vals]

g=open(out, "w+")
for val in vals:
  g.write(str(val) + "\n")

