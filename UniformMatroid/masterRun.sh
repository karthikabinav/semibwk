#!/bin/bash

n=50
T=600
K=5

#Varying T n=50, K=10
#for i in `seq 100 700 4700`;
#do
#  echo "Started $i"
#  bash runCodes.sh $n $i $K > ../Output/Output_varyingT_uniform_$i 
#  echo "Done $i"
#done

#for i in `seq 100 250 6000`;
#do
#  echo "Started $i"
#  bash runCodes_dynamicPricing.sh $n $i $K > ../Output/Output_varyingT_dynamicPricing_$i 
#  echo "Done $i"
#done


#Varying n T=4700, K=10
#for i in `seq 25 25 650`;
#do
#  for j in `seq 1200 700 4700`;
#  do  
#    echo "Started $i $j"
#    bash runCodes.sh $i $T $K > ../Output/Output_varyingn_uniform_"$i"_"$j" 
#    echo "Done $i $j"
#  done
#done

#Varying n T=4700, K=10
#for i in `seq 25 25 270`;
#do
#    j=1200
#    echo "Started $i $j"
#    bash runCodes_dynamicPricing_varyingn.sh $i $T $K > ../Output/Output_varyingn_dynamicPricing_"$i"_"$j" 
#    echo "Done $i $j"
#    j=4700
#    echo "Started $i $j"
#    bash runCodes_dynamicPricing_varyingn.sh $i $T $K > ../Output/Output_varyingn_dynamicPricing_"$i"_"$j" 
#    echo "Done $i $j"
#done

#Varying K n=50
#for i in `seq 2 5 32`;
#do
#    j=1200
#    echo "Started $i $j"
#    bash runCodes_dynamicPricing.sh $n $T $i > ../Output/Output_varyingk_uniform_"$i"_"$j" 
#    echo "Done $i $j"
#    j=4700
#    echo "Started $i $j"
#    bash runCodes_dynamicPricing.sh $n $T $i > ../Output/Output_varyingk_uniform_"$i"_"$j" 
#    echo "Done $i $j"

#done

#Varying K T=4700, n=50
#for i in `seq 2 5 32`;
#do
#  for j in `seq 1200 700 4700`;
#  do
#    echo "Started $i $j"
#    bash runCodes_dynamicPricing.sh $n $T $i > ../Output/Output_varyingk_dynamicPricing_"$i"_"$j" 
#    echo "Done $i $j"
#done
#done

#Running the comparision algorithm for varying n and T
K=2
#for i in `seq 10 5 40`;
#do
#    t=1200
#    echo "Started $i $t"
#    bash runCodes.sh $i $t $K > ../Output/Output_Uniformcomparision_"$i"_"$t" 
#    echo "Done $i $t"
#    t=4700
#    echo "Started $i $t"
#    bash runCodes.sh $i $t $K > ../Output/Output_Uniformcomparision_"$i"_"$t" 
#    echo "Done $i $t"

#done

K=2
for i in `seq 10 5 40`;
do
  (t=1200;B=2000;echo "Started $i $t $B";bash runCodes_NonUniform.sh $i $t $K $B > ../Output/Output_NonUniformcomparision_"$i"_"$t"_"$B"; echo "Done $i $t $B") &
  (t=1200;B=5000;echo "Started $i $t $B";bash runCodes_NonUniform.sh $i $t $K $B > ../Output/Output_NonUniformcomparision_"$i"_"$t"_"$B"; echo "Done $i $t $B") &
  (t=4700;B=5000;echo "Started $i $t $B";bash runCodes_NonUniform.sh $i $t $K $B > ../Output/Output_NonUniformcomparision_"$i"_"$t"_"$B"; echo "Done $i $t $B") &
  (t=4700;B=8000;echo "Started $i $t $B";bash runCodes_NonUniform.sh $i $t $K $B > ../Output/Output_NonUniformcomparision_"$i"_"$t"_"$B"; echo "Done $i $t $B") &
done

K=2
#for i in `seq 10 5 40`;
#do
#    t=1200
#    echo "Started $i $t"
#    bash runCodes_dynamicPricing_deterministic.sh $i $t $K > ../Output/Output_dynamicPricingDeterministiccomparision_"$i"_"$t" 
#    echo "Done $i $t"
#    t=4700
#    echo "Started $i $t"
#    bash runCodes_dynamicPricing_deterministic.sh $i $t $K > ../Output/Output_dynamicPricingDeterministiccomparision_"$i"_"$t" 
#    echo "Done $i $t"
#done

K=2
#for i in `seq 10 5 40`;
#do
#    t=1200
#    echo "Started $i $t"
#    bash runCodes_dynamicPricing_Correlated.sh $i $t $K > ../Output/Output_dynamicPricingCorrelatedcomparision_"$i"_"$t" 
#    echo "Done $i $t"
#    t=4700
#    echo "Started $i $t"
#    bash runCodes_dynamicPricing_Correlated.sh $i $t $K > ../Output/Output_dynamicPricingCorrelatedcomparision_"$i"_"$t" 
#    echo "Done $i $t"
#done


#for i in `seq 10 5 40`;
#do
#    t=1200
#    echo "Started $i $t"
#    bash runCodes_dynamicPricing_comparing.sh $i $t $K > ../Output/Output_dynaicPricingcomparision_"$i"_"$t" 
#    echo "Done $i $t"
#    t=4700
#    echo "Started $i $t"
#    bash runCodes_dynamicPricing_comparing.sh $i $t $K > ../Output/Output_dynaicPricingcomparision_"$i"_"$t" 
#    echo "Done $i $t"

#done


#K=5
#for i in `seq 1000 700 5000`;
#do
#   echo "Started $i"
#    bash runCodes.sh $n $i $K $i > ../Output/Output_uniformBFull_"$i" 
#  echo "Done $i"
#done
