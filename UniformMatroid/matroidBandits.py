import sys
import numpy as np
from numpy import array
import math
import cvxopt
from cvxopt import matrix,spmatrix, solvers
import random

#def playArm():

def rad(t, N):
  A= math.sqrt(float(2*math.log(1+t))/float(N))
  return A

def out_of_resources():
  for i in xrange(len(consumption)):
    if consumption[i]>B:
      return True
  return False

def getAction():
  #Construct LP
  W=list()
  indices=list()
  for i in xrange(n):
    W.append(float(UCB[i]))
    indices.append(i)
  
  combined=zip(W,indices)
  combined.sort(reverse=True)

  return [b for a,b in combined[:K]]
  
n=int(sys.argv[1])
T=int(sys.argv[2])
d=int(sys.argv[3])
B=int(sys.argv[4])
delta=float(sys.argv[5])
K=int(sys.argv[6])
f_feedback=sys.argv[7]


f=open(f_feedback, "r")

feedback = [[[0 for k in xrange(d+1)] for j in xrange(n)] for i in xrange(T)]

arm=0
time=0

for line in f:
  vals=line.split(",")
  if arm==n:
    arm=0
    time+=1
  for i in xrange(len(vals)):
    feedback[time][arm][i]=float(vals[i].split("\n")[0])
  arm+=1

alpha=np.log((n*d*T)/delta)
#Exploration phase
N = [0 for j in xrange(n)]
average=[float(0) for j in xrange(n)]
UCB = [float(0) for j in xrange(n)]

reward=0
consumption=[float(0) for k in xrange(d)]

for t in xrange(n):
  if out_of_resources():
    break

  N[t]+=1
  observed=feedback[t][t]
  average[t]=(average[t]*(N[t]-1) + observed[0])/N[t]
  UCB[t]=min(1, average[t] + rad(t, N[t]) )

  for l in xrange(len(observed)):
    if l==0:
      reward+=observed[0]
    else:
      consumption[l-1]+=observed[l]

for t in xrange(n, T):
  if out_of_resources():
    break
  for x in xrange(n):
    UCB[x]=min(1, average[x] + rad(t, N[x]) )
  
  played=getAction()
  #Update the vectors
  for x in played:
    N[x]+=1
    observed=feedback[t][x]
   
    average[x]=(average[x]*(N[x]-1) + observed[0])/N[x]
    for l in xrange(len(observed)):
      if l==0:
        reward+=observed[0]
      else:
        consumption[l-1]+=observed[l]
    #print UCB
    #print LCB

print reward
