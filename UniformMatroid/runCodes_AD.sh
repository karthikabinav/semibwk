#!/bin/bash

#Calls to various codes. Uncomment and use 
n=$1
T=$2
B=$3

folder=$4

d=1
K=2
delta=0.01

append=$$

#Generate Random Inputs
#filename=Feedback_"$n"_"$T".in
filename=Feedback_"$T"_1.in

full_path=../Input/$folder/$filename

i=1
matlab -nodesktop -nosplash -nojvm -r "AD14_sampling('$full_path','../Output/AD16SamplingUniform_"$append"_"$folder"_"$i"',$n, $T, $d, $K, $B, $(head -n 1 ../Input/"$folder"/OPT_"$T"_1));quit"

mv ../Output/AD16SamplingUniform_"$append"_"$folder"_"$i" ../Output/Output_AD16Uniform_"$folder"_"$n"_"$B".out


