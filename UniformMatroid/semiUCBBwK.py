import sys
import numpy as np
from numpy import array
import math
import cvxopt
from cvxopt import matrix,spmatrix, solvers
import random

def rad(X, N):
  A= math.sqrt(float(alpha*X)/float(N))
  B=float(alpha)/float(N)
  return A+B

def out_of_resources():
  for i in xrange(len(consumption)):
    if consumption[i]>B:
      return True
  return False

def rand_round_uniform_matroid(vec):
  pos1=0
  pos2=1
  
  while pos1<n and pos2<n:
    if vec[pos1] == 1 or vec[pos1] == 0:
      temp=pos2
      pos1=pos2
      pos2=temp+1
      continue
    if vec[pos2] == 1 or vec[pos2] == 0:
      pos2+=1
      continue
      
    a=vec[pos1]
    b=vec[pos2]
    alpha = min(1-a,b)
    beta=min(a, 1-b)

    r=random.random()
    if r<=beta/(alpha+beta):
      vec[pos1]+=alpha
      vec[pos2]-=alpha
    else:
      vec[pos1]-=beta
      vec[pos2]+=beta
    
  ret=list()
  for i in xrange(len(vec)):
    if vec[i]>=0.9999:
      ret.append(i)
  return ret
def getAction():
  #Construct LP
  W=list()
  for i in xrange(n):
    W.append(-1 * float(UCB[i][0]))
  b=list()
  
  vals=list()
  I=list()
  J=list()
  
  #LCB constraint
  for i in xrange(d):
    b.append(B/float(T))
    for j in xrange(n):
      vals.append(LCB[j][i+1])
      I.append(i)
      J.append(j)

  #Matroid constraint
  b.append(K)
  for j in xrange(n):
    vals.append(1)
    I.append(d)
    J.append(j)
  i
  #Variable bounds constraint
  for j in xrange(n):
    b.append(1)
    vals.append(1)
    I.append(d+j+1)
    J.append(j)
    
  for j in xrange(n):
    b.append(0)
    vals.append(-1)
    I.append(d+n+j+1)
    J.append(j)

  A=spmatrix(vals, I, J)
  Wc=matrix(W)
  bc=matrix(b)
  #Solve LP
  solvers.options['show_progress'] = False
  sol=solvers.lp(Wc, A, bc) 
  vec = sol['x']
  #Randomized Rounding
  return rand_round_uniform_matroid(vec)

n=int(sys.argv[1])
T=int(sys.argv[2])
d=int(sys.argv[3])
B=int(sys.argv[4])
delta=float(sys.argv[5])
K=int(sys.argv[6])
f_feedback=sys.argv[7]

#alpha=np.log((n*d*T)/delta)
alpha=5
#epsilon=math.sqrt(alpha*n/float(B)) + alpha*n/float(B) + math.sqrt(alpha*n*T)/float(B)
#B=B*(1-epsilon)

f=open(f_feedback, "r")

feedback = [[[0 for k in xrange(d+1)] for j in xrange(n)] for i in xrange(T)]

arm=0
time=0

for line in f:
  vals=line.split(",")
  if arm==n:
    arm=0
    time+=1
  for i in xrange(len(vals)):
    feedback[time][arm][i]=float(vals[i].split("\n")[0])
  arm+=1

#Exploration phase
N = [0 for j in xrange(n)]
average=[[float(0) for k in xrange(d+1)] for j in xrange(n)]
UCB = [[float(0) for k in xrange(d+1)] for j in xrange(n)]
LCB = [[float(0) for k in xrange(d+1)] for j in xrange(n)]

reward=0
consumption=[float(0) for k in xrange(d)]

for t in xrange(n):
  if out_of_resources():
    break

  N[t]+=1
  observed=feedback[t][t]
  for l in xrange(len(observed)):
    average[t][l]=(average[t][l]*(N[t]-1) + observed[l])/N[t]
    UCB[t][l]=min(1, average[t][l] + rad(average[t][l], N[t]) )
    LCB[t][l]=max(0, average[t][l] - rad(average[t][l], N[t]) )
    if l==0:
      reward+=observed[0]
    else:
      consumption[l-1]+=observed[l]
for t in xrange(n, T):
  if out_of_resources():
    break
  played=getAction()
  #Update the vectors
  assert len(played)<=K
  for x in played:
    N[x]+=1
    observed=feedback[t][x]
  
    for l in xrange(len(observed)):
      average[x][l]=(average[x][l]*(N[x]-1) + observed[l])/N[x]
      UCB[x][l]=min(1, average[x][l] + rad(average[x][l], N[x]) )
      LCB[x][l]=max(0, average[x][l] - rad(average[x][l], N[x]) )
      if l==0:
        reward+=observed[0]
      else:
        consumption[l-1]+=observed[l]
    #print UCB
    #print LCB

print reward
