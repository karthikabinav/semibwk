#!/bin/bash

# Lines that begin with #SBATCH specify commands to be used by SLURM for scheduling

#SBATCH --job-name=UniMB                              # sets the job name
#SBATCH --output UniMB.out.%j                              # indicates a file to redirect STDOUT to; %j is the jobid 
#SBATCH --error UniMB.out.%j                               # indicates a file to redirect STDERR to; %j is the jobid
#SBATCH --time=08:59:59                                         # how long you think your job will take to complete; format=hh:mm:ss
#SBATCH --partition dpart
#SBATCH --gres=gpu:0
#SBATCH --nodes=4                                               # number of nodes to allocate for your job
#SBATCH --ntasks=28                                              # request 4 cpu cores be reserved for your node total
#SBATCH --ntasks-per-node=7                                    # request 2 cpu cores be reserved per node
#SBATCH --mem=50gb                                               # memory required by job; if unit is not specified MB will be assumed

source ../project_files/bin/activate
module load matlab/2016b

K=2
declare -a arr=(6 26)
i=26
#for i in "${arr[@]}";
#do
for tt in `seq 1000 1000 6000`;
do
  (t=$tt;B=$(($t/2));echo "Started $i $t $B";srun --exclusive -N1 -n1 bash runCodes_MB.sh $i $t $B DynamicAssortment ; echo "Done $i $t $B") &
done
#done


#for i in "${arr[@]}";
#do
#for tt in `seq 1000 1000 6000`;
#do
#  (t=$tt;B=$(($t/2));echo "Started $i $t $B";srun --exclusive -N1 -n1 bash runCodes_MB.sh $i $t $B DA2 ; echo "Done $i $t $B") &
#done
#done


#for i in "${arr[@]}";
#do
#  (t=6000;B=1000;echo "Started $i $t $B";srun --exclusive -N1 -n1 bash runCodes_MB.sh $i $t $B DynamicAssortment1 ; echo "Done $i $t $B") &
#  (t=6000;B=3000;echo "Started $i $t $B";srun --exclusive -N1 -n1 bash runCodes_MB.sh $i $t $B DynamicAssortment1 ; echo "Done $i $t $B") &
#  (t=6000;B=5000;echo "Started $i $t $B";srun --exclusive -N1 -n1 bash runCodes_MB.sh $i $t $B DynamicAssortment1 ; echo "Done $i $t $B") &
#done
#for i in "${arr[@]}";
#do
#  (t=6000;B=1000;echo "Started $i $t $B";srun --exclusive -N1 -n1 bash runCodes_MB.sh $i $t $B DynamicAssortment2 ; echo "Done $i $t $B") &
#  (t=6000;B=3000;echo "Started $i $t $B";srun --exclusive -N1 -n1 bash runCodes_MB.sh $i $t $B DynamicAssortment2 ; echo "Done $i $t $B") &
#  (t=6000;B=5000;echo "Started $i $t $B";srun --exclusive -N1 -n1 bash runCodes_MB.sh $i $t $B DynamicAssortment2 ; echo "Done $i $t $B") &
#done
#for i in "${arr[@]}";
#do
#  (t=6000;B=1000;echo "Started $i $t $B";srun --exclusive -N1 -n1 bash runCodes_MB.sh $i $t $B DynamicAssortment3 ; echo "Done $i $t $B") &
#  (t=6000;B=3000;echo "Started $i $t $B";srun --exclusive -N1 -n1 bash runCodes_MB.sh $i $t $B DynamicAssortment3 ; echo "Done $i $t $B") &
#  (t=6000;B=5000;echo "Started $i $t $B";srun --exclusive -N1 -n1 bash runCodes_MB.sh $i $t $B DynamicAssortment3 ; echo "Done $i $t $B") &
#done
#for i in "${arr[@]}";
#do
#  (t=6000;B=1000;echo "Started $i $t $B";srun --exclusive -N1 -n1 bash runCodes_MB.sh $i $t $B DynamicAssortment4 ; echo "Done $i $t $B") &
#  (t=6000;B=3000;echo "Started $i $t $B";srun --exclusive -N1 -n1 bash runCodes_MB.sh $i $t $B DynamicAssortment4 ; echo "Done $i $t $B") &
#  (t=6000;B=5000;echo "Started $i $t $B";srun --exclusive -N1 -n1 bash runCodes_MB.sh $i $t $B DynamicAssortment4 ; echo "Done $i $t $B") &
#done
#for i in "${arr[@]}";
#do
  #(t=6000;B=1000;echo "Started $i $t $B";srun --exclusive -N1 -n1 bash runCodes_MB.sh $i $t $B Correlated; echo "Done $i $t $B") &
  #(t=6000;B=3000;echo "Started $i $t $B";srun --exclusive -N1 -n1 bash runCodes_MB.sh $i $t $B Correlated; echo "Done $i $t $B") &
 # (t=6000;B=5000;echo "Started $i $t $B";srun --exclusive -N1 -n1 bash runCodes_MB.sh $i $t $B Correlated; echo "Done $i $t $B") &
#done


wait
