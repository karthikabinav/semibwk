import math
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import sys


X=[10, 20, 30, 40, 50, 75, 100]
X1=[10, 20, 30, 40]

AD=list()
MB=list()
PD=list()
semi=list()

for n in X1:
    f=open("./FinalOutputs/OutputAD16SamplingTimes_51313_DynamicAssortment3_" + str(n), "r")
    score=0
    total=0
    for line in f:
        score=float(line.split("\n")[0])
        total+=1
    score=score/total
    AD.append(np.log(1000000*score))

for n in X:
    f=open("./FinalOutputs/OutputTimes_DynamicAssortment3_" + str(n), "r")
    score=0
    total=0
    for line in f:
        score=float(line.split("\n")[0])
        total+=1
    #score=score/total
    semi.append(np.log(1000000*score))

for n in X:
    f=open("./FinalOutputs/OutputTimesPD_DynamicAssortment3_" + str(n), "r")
    score=0
    total=0
    for line in f:
        score=float(line.split("\n")[0])
        total+=1
    #score=score/total
    PD.append(np.log(1000000*score))

for n in X:
    f=open("./FinalOutputs/OutputTimesmatroidBandits_DynamicAssortment3_" + str(n), "r")
    score=0
    total=0
    for line in f:
        score=float(line.split("\n")[0])
        total+=1
    #score=score/total
    MB.append(np.log(1000000*(score)))

print(semi)

'''
plt.hold(True)
plt.plot(X, semi, label='SemiBwK-RRS', linestyle='-', color='red', linewidth=5)
plt.plot(X, MB, label='OMM', linestyle='-', color='blue', linewidth=3)
plt.plot(X, PD, label='PD-BwK', linestyle='--', color='green', linewidth=5)
plt.plot(X1, AD, label='linCBwK-sample', linestyle='--', color='black', linewidth=5)

axis_font={'size' : 30}

plt.title("Comparision of per-step running time of various algorithms as n varies", **axis_font)
plt.xlabel('Value of n', **axis_font)
plt.ylabel('log(micro seconds per step)', **axis_font)
font = {'family' : 'normal',
        'weight' : 'bold',
        'size'   : 22}

matplotlib.rc('font', **font)
plt.legend(loc='upper right')
plt.show()

'''
