import math
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import sys

OPT=list()

ALG=list()
PD=list()
MB=list()
AD=list()
X=[1000, 2000, 3000, 4000, 5000, 6000]

n=sys.argv[1]
dataset=sys.argv[2]
matroid=sys.argv[3]

## NonUniform distribution
if matroid=="partition":
    BASE="./FinalOutputs/"
    for t in X:
        f=open(BASE + dataset + "/OPT_" + str(n) + "_" + str(t) + "_" + str(t/2), "r")
        for line in f:
            OPT.append(t*float(line.split("\n")[0]))
    for t in X:
        f=open(BASE + "Output_semiUCBBwK_" +dataset + "_" + str(n) + "_" + str(t/2) + ".out", "r")
        last=0
        for line in f:
            last=float(line.split("\n")[0])
        ALG.append(last)
    for t in X:    
        f=open(BASE + "Output_PDBWK_" +dataset + "_" + str(n) + "_" + str(t/2) + ".out", "r")
        last=0
        for line in f:
            last=float(line.split("\n")[0])
        PD.append(last)
    for t in X:    
        f=open(BASE + "Output_MB_" +dataset + "_" + str(n) + "_" + str(t/2) + ".out", "r")
        last=0
        for line in f:
            last=float(line.split("\n")[0])
        MB.append(last)
    for t in X:
        f=open(BASE + "Output_AD16_" +dataset + "_" + str(n) + "_" + str(t/2) + ".out", "r")
        last=0
        for line in f:
            last=float(line.split("\n")[0])
        AD.append(last)
else:
    BASE="./FinalOutputs/"
    for t in X:
        f=open(BASE + dataset + "/OPTUM_" + str(n) + "_" + str(t) + "_" + str(t/2), "r")
        for line in f:
            OPT.append(t*float(line.split("\n")[0]))
    for t in X:
        f=open(BASE + "Output_semiUCBBwKUniform_" +dataset + "_" + str(n) + "_" + str(t/2) + ".out", "r")
        last=0
        for line in f:
            last=float(line.split("\n")[0])
        ALG.append(last)
    for t in X:    
        f=open(BASE + "Output_PDBWKUniform_" +dataset + "_" + str(n) + "_" + str(t/2) + ".out", "r")
        last=0
        for line in f:
            last=float(line.split("\n")[0])
        PD.append(last)
    for t in X:    
        f=open(BASE + "Output_MBUniform_" +dataset + "_" + str(n) + "_" + str(t/2) + ".out", "r")
        last=0
        for line in f:
            last=float(line.split("\n")[0])
        MB.append(last)
    for t in X:
        f=open(BASE + "Output_AD16Uniform_" +dataset + "_" + str(n) + "_" + str(t/2) + ".out", "r")
        last=0
        for line in f:
            last=float(line.split("\n")[0])
        AD.append(last)

    
print len(X)
print len(ALG)
print len(PD)
print len(MB)
print len(AD)
plt.hold(True)
plt.plot(X, OPT, label='OPT', linestyle='-.', color='brown', linewidth=5)
plt.plot(X, ALG, label='SemiBwK-RRS', linestyle='-', color='red', linewidth=5)
plt.plot(X, MB, label='OMM', linestyle='-', color='blue', linewidth=3)
plt.plot(X, PD, label='PD-BwK', linestyle='--', color='green', linewidth=5)
plt.plot(X, AD, label='linCBwK-sample', linestyle='--', color='black', linewidth=5)

axis_font={'size' : 30}

plt.title("Comparision of various algorithms as T varies (n=" + str(n) + " , K=2,  d=1, B=T/2)", **axis_font)
plt.xlabel('Value of T', **axis_font)
plt.ylabel('Reward', **axis_font)
font = {'family' : 'normal',
        'weight' : 'bold',
        'size'   : 26}

matplotlib.rc('font', **font)
plt.legend(loc='upper left')
#plt.ylim([4.5, 5.5])
plt.show()


