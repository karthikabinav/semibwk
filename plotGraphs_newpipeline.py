import math
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import sys




ALG=list()
PD=list()
MB=list()
AD=list()
X=list()

n=sys.argv[1]
B=sys.argv[2]
dataset=sys.argv[3]
matroid=sys.argv[4]

## NonUniform distribution
if matroid=="partition":
    BASE="./Output_new/"
    for i in xrange(200, 6000):
        X.append(i)
    f=open(BASE + dataset + "/OPT_" + str(n) + "_6000_" + str(B), "r")
    for line in f:
        OPT=float(line.split("\n")[0])
    f=open(BASE + "Output_semiUCBBwK_" +dataset + "_" + str(n) + "_" + str(B) + ".out", "r")
    for line in f:
        ALG.append(OPT-float(line.split("\n")[0]))
        
    f=open(BASE + "Output_PDBWK_" +dataset + "_" + str(n) + "_" + str(B) + ".out", "r")
    for line in f:
        PD.append(OPT-float(line.split("\n")[0]))
        
    f=open(BASE + "Output_MB_" +dataset + "_" + str(n) + "_" + str(B) + ".out", "r")
    for line in f:
        curStep=OPT-float(line.split("\n")[0])
        MB.append(curStep)
    f=open(BASE + "Output_AD16_" +dataset + "_" + str(n) + "_" + str(B) + ".out", "r")
    last=0
    for line in f:
        curStep=max(0, OPT-float(line.split("\n")[0]))
        AD.append(curStep)
        last=float(line.split("\n")[0])
    for i in xrange(len(AD),5800):
        AD.append(max(0, OPT-last))

else:
    BASE="./Output_new/"
    for i in xrange(200, 6000):
        X.append(i)
    f=open(BASE + dataset + "/OPT_" + str(n) + "_6000_" + str(B), "r")
    for line in f:
        OPT=float(line.split("\n")[0])
    f=open(BASE + "Output_semiUCBBwKUniform_" +dataset + "_" + str(n) + "_" + str(B) + ".out", "r")
    for line in f:
        ALG.append(OPT-float(line.split("\n")[0]))
        
    f=open(BASE + "Output_PDBWKUniform_" +dataset + "_" + str(n) + "_" + str(B) + ".out", "r")
    for line in f:
        PD.append(OPT-float(line.split("\n")[0]))
        
    f=open(BASE + "Output_MBUniform_" +dataset + "_" + str(n) + "_" + str(B) + ".out", "r")
    for line in f:
        curStep=OPT-float(line.split("\n")[0])
        MB.append(curStep)
    f=open(BASE + "Output_AD16Uniform_" +dataset + "_" + str(n) + "_" + str(B) + ".out", "r")
    last=0
    for line in f:
        curStep=max(0, OPT-float(line.split("\n")[0]))
        AD.append(curStep)
        last=float(line.split("\n")[0])
    for i in xrange(len(AD),5800):
        AD.append(max(0, OPT-last))

   
print len(X)
print len(ALG)
print len(PD)
print len(MB)
print len(AD)
plt.hold(True)
plt.plot(X, ALG, label='SemiBwKUCB', linestyle='-', color='red', linewidth=5)
plt.plot(X, MB, label='MB', linestyle='-', color='blue', linewidth=3)
plt.plot(X, PD, label='PD', linestyle='--', color='green', linewidth=5)
plt.plot(X, AD, label='AD', linestyle='--', color='black', linewidth=5)

axis_font={'size' : 30}

plt.title("Comparision of various algorithms as T varies (n=" + str(n) + " , K=2,  d=5, B=" + str(B) + ")", **axis_font)
plt.xlabel('Value of T', **axis_font)
plt.ylabel('Average Regret = 1/T*(OPT-ALG)', **axis_font)
font = {'family' : 'normal',
        'weight' : 'bold',
        'size'   : 26}

matplotlib.rc('font', **font)
plt.legend(loc='upper right')
#plt.ylim([4.5, 5.5])
plt.show()


